#include<stdlib.h>
int main(int argc,char*argv[])
{
    FILE*fp1,*fp2;
    int c;
    if (argc != 3)
    {
        fprintf(stderr,"%s:wrong number of the argument.\n", argv[0]);
        exit(1);
    }
    else if((fp1=fopen(argv[1],"r"))==NULL)
    {
        fprintf(stderr,"%s:error in opening file for reading.\n", argv[1]);
        exit(2);
    }
    else if((fp2=fopen(argv[2],"w"))==NULL)
    {
        fprintf(stderr,"%s:error in opening file for writing.\n", argv[2]);
        exit(3);
    }
    while((c=fgetc(fp1)) !=EOF)
      fputc(c,fp2);
    fclose(fp1);fclose(fp2);
    exit(0);
}
OUTPUT:
./a.out:wrong number of the argument.                                                                            
                                    