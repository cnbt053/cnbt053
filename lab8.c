#include <stdio.h>
struct student {
    char name[50];
    char sect[50];
    char dept[50];
    int roll;
} s[10];

int main() 
{
    int i;
    printf("Entering information of students:\n");
    for (i = 0; i < 1; i++) 
    {
        printf("\nEnter roll no.:\n");
        scanf("%d",&s[i].roll);
        printf("Enter student name:\n");
        scanf("%s",s[i].name);
        printf("Enter section:\n");
        scanf("%s",s[i].sect);
        printf("Enter department:\n");
        scanf("%s",s[i].dept);
       
    }
    printf("\nDisplaying Information:\n");
    for (i = 0; i < 1; ++i) 
    {
        
        printf("\nRoll number: %d\n",s[i].roll);
        printf("Name:");
        puts(s[i].name);
        printf("Section:");
        puts(s[i].sect);
        printf("Department:");
        puts(s[i].dept);
    }
    return 0;
}

Output:
Entering information of students:                                                                                                      
                                                                                                                                       
Enter roll no.:                                                                                                                        
5                                                                                                                                     
Enter student name:
Surabhi.V
Enter section:             
CN                                                                                                                                     
Enter department:                                                                                                                      
Biotech                                                                                                                                
                                                                                                                                       
Displaying Information:                                                                                                                
                                                                                                                                       
Roll number: 5 
Name:Surabhi.V                                                                                                                             
Section:CN                                                                                                                             
Department:Biotech
