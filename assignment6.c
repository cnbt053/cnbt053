#include<stdio.h>
void main()
{
    int i,n,fact=1;
    printf("Enter a no:\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        fact=fact*i;
    }
    printf("Factorial:%d",fact);
}

Output:
Enter a no:                                                                                                           
5                                                                                                                     
Factorial:120  

#include<stdio.h>
void main()
{
    int n,dig,rev=0;
    printf("Enter a no:\n");
    scanf("%d",&n);
    while(n!=0)
    {
        dig=n%10;
        rev=rev*10+dig;
        n=n/10;
    }
    printf("Reverse:%d",rev);
}

Output:
Enter a no:                                                                                                           
203                                                                                                                   
Reverse:302 


#include<stdio.h>
#include<math.h>
void main()
{
    int i,n;
    float sum=0.0;
    printf("Enter the value of n:\n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        sum=sum+(1/pow(i,2));
    }
    printf("Sum:%f",sum);
}

Output:
Enter the value of n:                                                                                                 
5                                                                                                                     
Sum:1.463611  


#include<stdio.h>
void main()
{
    int i,j;
    for(i=1;i<=5;i++)
    {
        for(j=1;j<=i;j++)
        {
            printf("%d",i);
        }
        printf("\n");
    }
}

Output:
1                                                                                                                     
22                                                                                                                    
333                                                                                                                   
4444                                                                                                                  
55555  

#include<stdio.h>
void main()
{
    int i,j;
    for(i=65;i<=70;i++)
    {
        for(j=65;j<=i;j++)
        {
            printf("%c",j);
        }
        printf("\n");
    }
}

Output:
A                                                                                                                     
AB                                                                                                                    
ABC                                                                                                                   
ABCD                                                                                                                  
ABCDE                                                                                                                 
ABCDEF 

#include <stdio.h>
void main()
{
    int n1, n2, i, gcd;
    printf("Enter two integers:\n");
    scanf("%d %d", &n1, &n2);
    for(i=1;i<=n1 && i<=n2;i++)
    {
        if(n1%i==0 && n2%i==0)
            gcd=i;
    }
    printf("GCD:%d",gcd);
}

Output:
Enter two integers:                                                                                                   
65                                                                                                                    
15                                                                                                                    
GCD:5 